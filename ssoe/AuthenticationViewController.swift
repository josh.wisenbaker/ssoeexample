//
//  AuthenticationViewController.swift
//  ssoe
//
//  Created by Joel Rennich on 6/4/20.
//  Copyright © 2020 Jamf. All rights reserved.
//

import Cocoa
import AuthenticationServices
import WebKit

class AuthenticationViewController: NSViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var cancelButton: NSButton!
    @IBOutlet weak var injectButton: NSButton!
    
    var authorizationRequest: ASAuthorizationProviderExtensionAuthorizationRequest?
    var errorText: String?

    override func viewDidAppear() {
        if let url = authorizationRequest?.url {
            let newURLString = "https://login.microsoftonline.com" + url.path + "?" + (url.query ?? "")
            webView.load(URLRequest.init(url: URL.init(string: newURLString)!))
        } else {
            webView.loadHTMLString(genErrorHTML(), baseURL: nil)
        }
    }

    override var nibName: NSNib.Name? {
        return NSNib.Name("AuthenticationViewController")
    }
    
    // This tells the OS we're not going to handle this request and pass it back to the calling app unmolested
    
    @IBAction func clickCancel(_ sender: Any) {
        authorizationRequest?.doNotHandle()
    }
    
    // Here's where we take any cookies that have been set in this webview and pass them back
    // via a 302 redirect to the original application.
    // While this isn't foolproof, in most cases this should allow the process to proceed.
    // Note... there are MUCH better ways to do this, but this is good for our example here.
    
    @IBAction func clickInject(_ sender: Any) {
        
        if let url = webView.url,
            let authURL = authorizationRequest?.url {
            webView.configuration.websiteDataStore.httpCookieStore.getAllCookies({ cookies in
                let headers: [String:String] = [
                    "Location": url.absoluteString,
                    "Set-Cookie": self.combineCookies(cookies: cookies)
                ]
                if let response = HTTPURLResponse.init(url: authURL, statusCode: 302, httpVersion: nil, headerFields: headers) {
                    self.authorizationRequest?.complete(httpResponse: response, httpBody: nil)
                }
            })
        }
    }
    
    // a way of showing how you can arbitrarily return a redirect anywhere
    
    @IBAction func clickApple(_ sender: Any) {
        if let url = URL.init(string: "https://www.apple.com"),
            let authURL = authorizationRequest?.url {
            let headers: [String:String] = [
                "Location": url.absoluteString
            ]
            
            if let response = HTTPURLResponse.init(url: authURL, statusCode: 302, httpVersion: nil, headerFields: headers) {
                self.authorizationRequest?.complete(httpResponse: response, httpBody: nil)
            }
        }
    }
    
    // Cookies given to us from the WebView are an array, we need to convert this into a single string
    // to return as part of the 302 redirect. This is a simple func to do that.
    
    fileprivate func combineCookies(cookies: [HTTPCookie]) -> String {
        let dateFormatter = ISO8601DateFormatter.init()
        var cookiesStrings = [String]()
        for cookie in cookies {
          var cookieString = [String]()
          cookieString.append("\(cookie.name)=\(cookie.value)")
          cookieString.append("domain=\(cookie.domain)")
          cookieString.append("path=\(cookie.path)")
          if let expires = cookie.expiresDate {
            cookieString.append("expires=\(dateFormatter.string(from: expires))")
          }
          if cookie.isSecure {
            cookieString.append("secure")
          }
          if cookie.isHTTPOnly {
            cookieString.append("httponly")
          }
          if let sameSite = cookie.sameSitePolicy {
            cookieString.append("SameSite=\(sameSite.rawValue)")
          }
          cookiesStrings.append(cookieString.joined(separator: "; "))
        }
        return cookiesStrings.joined(separator: ", ")
    }
    
    // some boilerplate html to load into the WebView in case we hit an error
    
    private func genErrorHTML() -> String {
        return """
        <!DOCTYPE html>
        <html>
        <head>
        <title>
        Success!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        body {background-color:#ffffff;background-repeat:no-repeat;background-position:top left;background-attachment:fixed;}
        h1{font-family:Arial, sans-serif;color:#000000;background-color:#FFFFFF;}
        p {font-family:Georgia, serif;font-size:14px;font-style:normal;font-weight:normal;color:#000000;background-color:#FFFFFF;}
        </style>
        </head>
        <body>
        <h1>AuthRequest</h1>
        <p>\(authorizationRequest.debugDescription)</p>
        <h1>Error Message</h1>
        <p>\(errorText ?? "None")</p>
        </body>
        </html>
        """
    }
}

extension AuthenticationViewController: ASAuthorizationProviderExtensionAuthorizationRequestHandler {
    
    // this is the main entry point where the SSOE gets called
    
    public func beginAuthorization(with request: ASAuthorizationProviderExtensionAuthorizationRequest) {
        self.authorizationRequest = request
        
        // this allows for app bundles to be sent to the extension
        // SSOE will ignore requests from those apps
        
        if let exclusions = authorizationRequest?.extensionData["ExcludedApps"] as? [String],
            let callerBundle = authorizationRequest?.callerBundleIdentifier,
            exclusions.contains(callerBundle) {
            authorizationRequest?.doNotHandle()
            return
        }

         request.presentAuthorizationViewController(completion: { (success, error) in
             if error != nil {
                 request.complete(error: error!)
             }
         })
    }
}
